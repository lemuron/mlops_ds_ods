import click
import os
from typing import List

import boto3

@click.command()
@click.argument("repo_name", type=str)
@click.argument("branch", type=str)
@click.argument("input_path", type=str)
def lakefs_boto(repo_name: str, branch: str, input_path: str):
    host = "http://localhost:8766"
    username = "AKIAIOSFOLKFSSAMPLES"
    password = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"

    s3 = boto3.client('s3',
        endpoint_url=host,
        aws_access_key_id=username,
        aws_secret_access_key=password)

    with open(input_path, 'rb') as f:
        s3.put_object(Body=f, Bucket=repo_name, Key=os.path.join(branch, input_path))

if __name__ == "__main__":
    lakefs_boto()