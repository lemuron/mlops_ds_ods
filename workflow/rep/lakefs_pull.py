import json
import io
import click

import lakefs_client
from lakefs_client.client import LakeFSClient

@click.command()
@click.argument("repo_name", type=str)
@click.argument("branch_name", type=str)
def lakefs_pull(repo_name: str, branch_name: str):
    f = open('workflow/rep/lakefs_config.json')
    data = json.load(f)

    host = data["host"]
    username = data["username"]
    password = data["password"]

    configuration = lakefs_client.Configuration()
    configuration.host = host
    configuration.username = username
    configuration.password = password
    client = LakeFSClient(configuration)

    for obj in client.objects_api.list_objects(repo_name, branch_name).results:
        fl = client.objects_api.get_object(repo_name, branch_name, obj.path)
        print(obj.path)
        with open(obj.path, 'wb') as file:
            bf = io.BufferedWriter(file)
            content = fl.read()
            bf.write(content)


if __name__ == "__main__":
    lakefs_pull()