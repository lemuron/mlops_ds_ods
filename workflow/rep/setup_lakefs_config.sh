#!/bin/bash

# create config file
touch "lakefs_config.json"

# write config json to config file
echo "{\"host\": \"${1}\", \"username\": \"${2}\", \"password\": \"${3}\" }" >> lakefs_config.json