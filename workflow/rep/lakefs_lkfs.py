import json
import click
from typing import List

import lakefs_client
import lakefs_client.models as models
from lakefs_client.client import LakeFSClient


@click.command()
@click.argument("repo_name", type=str)
@click.argument("branch", type=str)
@click.argument("input_paths", type=click.Path(exists=True), nargs=-1)
def lakefs_lkfs(repo_name: str, branch:str, input_paths: List[str]):
    f = open('workflow/rep/lakefs_config.json')
    data = json.load(f)

    host = data["host"] # "http://localhost:8765"
    username = data["username"] # "AKIAIOSFOLKFSSAMPLES"
    password = data["password"] # "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"

    configuration = lakefs_client.Configuration()
    configuration.host = host
    configuration.username = username
    configuration.password = password
    client = LakeFSClient(configuration)

    for pth in input_paths:
        with open(f"{pth}", "rb") as f:
            client.objects_api.upload_object(
                repository=repo_name,
                branch=branch,
                path=pth,
                content=f
            )
        try:
            res = client.commits_api.commit(
                repository=repo_name,
                branch=branch,
                commit_creation=models.CommitCreation("Commit for {}".format(pth))
            )
        except lakefs_client.ApiException as e:
            if '{"message":"commit: no changes"}' not in e.body:
                print(e)
        print(pth)

if __name__ == "__main__":
    lakefs_lkfs()