import json
import pandas as pd
import click
import logging
import joblib
from sklearn.metrics import mean_absolute_error, mean_squared_error, roc_auc_score, accuracy_score, f1_score, precision_score, confusion_matrix
import numpy as np
# clearml experiments
from clearml import Task, Dataset
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.metrics import (
    ConfusionMatrixDisplay,
)
# from plotly.io import to_json

# for hydra
from hydra import compose, initialize
from omegaconf import OmegaConf
from hydra.utils import instantiate
log = logging.getLogger(__name__)

initialize(version_base=None, config_path="../../src/conf", job_name="test_models")

TARGET_FEATURE = "diam_category"


def initialize_hydra_conf(config_name: str, section:str, subsection: str):
    cfg = compose(config_name=config_name, overrides=["{}={}".format(section, subsection)])
    log.info(OmegaConf.to_yaml(cfg))
    return cfg


@click.command()
@click.argument("model_name", type=str)
def main(model_name: str):
    cfg = compose(config_name="config", overrides=["model={}".format(model_name)])
    log.info(OmegaConf.to_yaml(cfg))

    task = Task.init(project_name="Tree investigation", task_name="experiment_log_xgb_1")
    task.set_progress(0)
    # инициализация гиперпараметров на основе конфига для выбранной модели из hydra
    cfg_init = cfg["model"].copy()
    cfg_init.__delattr__("_target_")
    print(dict(cfg_init))
    # print(type(dict(cfg_init)))
    task.connect(dict(cfg_init))

    frame_path = Dataset.get(
        dataset_name="Raw data", dataset_project="Tree investigation"
    ).get_local_copy()

    pth = (frame_path + "/2015-street-tree-census-tree-data.csv").replace("\\", "/")
    print(pth)

    df = pd.read_csv(pth)

    cleaned_data = clean_data(df)
    task.upload_artifact(name="data_cleaned", artifact_object=cleaned_data)
    task.set_progress(20)

    train_df, test_df = prepare_datasets(cleaned_data)
    task.upload_artifact(name="train_data", artifact_object=train_df)
    task.upload_artifact(name="test_data", artifact_object=test_df)
    task.set_progress(50)

    model = train(model_name, train_df)
    joblib.dump(model, "models/model_" + model_name + ".clf", compress=True)
    task.set_progress(80)

    metrics, fig, c_matrix = test(model, test_df)
    task.set_progress(90)

    logger = task.get_logger()
    # for class_name, metrics in result.items():
    for metric, value in metrics.items():
        logger.report_single_value(model_name.join("_").join(metric), value)

    # logger.report_confusion_matrix(
    #     title='Confusion matrix',
    #     series='Test loss / correct',
    #     matrix=matrix,
    #     iteration=1
    # )

    logger.report_confusion_matrix("confusion_matrix", "ignored", matrix=c_matrix)
    # report the plotly figure
    logger.report_matplotlib_figure(title="confusion_matrix", series="'Test loss / correct'", iteration=0, figure=fig)  # report_plotly

    task.set_progress(100)


def clean_data(raw_data: pd.DataFrame):
    # оставляем наиболее информативные поля
    #     (в первой итерации убираем адресную составляющую, категорию описывающего пользователя, общее название вида и наличие защиты)
    df_subset_0 = raw_data[['tree_id', 'tree_dbh', 'stump_diam',
                    'curb_loc', 'status', 'health', 'spc_latin', 'steward',
                    'sidewalk', 'problems', 'root_stone',
                    'root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
                    'brch_light', 'brch_shoe', 'brch_other', ]]

    df_subset = df_subset_0.copy()
    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
                                               axis=1)
    df_subset.drop(["stump_diam", "tree_dbh"], axis=1, inplace=True)

    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
    # убираем поле статуса дерева
    df_subset_2.drop(["status"], axis=1, inplace=True)
    # также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
    df_subset_2.drop(["problems"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
    df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
    df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
    df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
    df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
    df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
    df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
    df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
    df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
    df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
    df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
    df_subset_3['health'] = df_subset_3['health'].astype("category")
    df_subset_3['steward'] = df_subset_3['steward'].astype("category")

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_3["diam_category"] = df_subset_3.apply(lambda x: 1 if int(x["diam"]) > 10 else 0, axis=1)

    # убираем характеристику - ширина ствола в дюймах
    df_subset_3.drop(["diam"], axis=1, inplace=True)
    # убираем характеристику - идентификатор дерева
    df_subset_3.drop(["tree_id"], axis=1, inplace=True)
    # устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
    df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")

    print(len(df_subset_3))
    return df_subset_3


def prepare_datasets(clean_data: pd.DataFrame):
    cfg = initialize_hydra_conf("config", "preprocessing", "ds_1")

    df2 = clean_data[list(cfg['preprocessing']['columns'])]

    train = df2.sample(frac=cfg['preprocessing']['fraction_to_select'], random_state=42)
    test = df2.drop(train.index)

    return train, test


def convert_df_with_categorial(dfwc, cat_features):
    return pd.get_dummies(dfwc, prefix=cat_features, columns=cat_features)


def train(model_name: str, train_df: pd.DataFrame):
    # initialize hydra conf
    cfg = compose(config_name="config", overrides=["model={}".format(model_name)])
    log.info(OmegaConf.to_yaml(cfg))
    print("in modeling 0")

    cat_features = list(filter(lambda item: item != TARGET_FEATURE, train_df.columns))
    train_df = train_df.astype("category")
    train_df[TARGET_FEATURE] = train_df[TARGET_FEATURE].astype(int)
    print("in modeling 1")

    x_train = convert_df_with_categorial(train_df.drop(TARGET_FEATURE, axis=1), cat_features)
    y_train = train_df[TARGET_FEATURE]
    print("in modeling 2")

    # instanitiate by hydra
    model = instantiate(cfg['model'], random_state=42)
    print("in modeling 3")

    model.fit(x_train, y_train)
    print("in modeling 4")

    return model


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title(f"Confusion Matrix")
    plt.tight_layout()
    return fig


def test(model, test_df: pd.DataFrame):
    # initialize hydra conf
    cfg = initialize_hydra_conf("config", "preprocessing", "ds_1")
    print("in testing 0")

    cat_features = list(filter(lambda item: item != TARGET_FEATURE, test_df.columns))

    test_df = test_df.astype("category")
    test_df[TARGET_FEATURE] = test_df[TARGET_FEATURE].astype(int)
    print("in testing 1")

    x_holdout = convert_df_with_categorial(test_df.drop(TARGET_FEATURE, axis=1), cat_features)
    y_holdout = test_df[TARGET_FEATURE]
    print("in testing 2")

    y_predicted = model.predict(x_holdout)
    score = dict(
        mae=round(mean_absolute_error(y_holdout, y_predicted), 5),
        rmse=round(mean_squared_error(y_holdout, y_predicted), 5),
        roc_auc=round(roc_auc_score(y_holdout, y_predicted, average="macro"), 5),
        accuracy=round(accuracy_score(y_holdout, y_predicted), 5),
        precision=round(precision_score(y_holdout, y_predicted), 5),
        f1=round(f1_score(y_holdout, y_predicted),5)
    )
    fig = conf_matrix(y_holdout, y_predicted)
    c_matrix = confusion_matrix(y_holdout, y_predicted)
    print("in testing 3")

    # matrix = np.array([y_holdout, y_predicted])
    return score, fig, c_matrix


if __name__ == "__main__":
    main()
