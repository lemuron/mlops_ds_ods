from clearml import PipelineController


def step_one(dataset_name="Raw data", dataset_project="Tree investigation"):
    import pandas as pd

    print("step_one")
    from clearml import Dataset
    frame_path = Dataset.get(
        dataset_name=dataset_name, dataset_project=dataset_project
    ).get_local_copy()

    pth = (frame_path + "/2015-street-tree-census-tree-data.csv").replace("\\", "/")
    print(pth)

    df = pd.read_csv(pth)

    # оставляем наиболее информативные поля
    #     (в первой итерации убираем адресную составляющую, категорию описывающего пользователя, общее название вида и наличие защиты)
    df_subset_0 = df[['tree_id', 'tree_dbh', 'stump_diam',
                      'curb_loc', 'status', 'health', 'spc_latin', 'steward',
                      'sidewalk', 'problems', 'root_stone',
                      'root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
                      'brch_light', 'brch_shoe', 'brch_other', ]]

    df_subset = df_subset_0.copy()
    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(
        lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
        axis=1)
    df_subset.drop(["stump_diam", "tree_dbh"], axis=1, inplace=True)

    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
    # убираем поле статуса дерева
    df_subset_2.drop(["status"], axis=1, inplace=True)
    # также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
    df_subset_2.drop(["problems"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
    df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
    df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
    df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
    df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
    df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
    df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
    df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
    df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
    df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
    df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
    df_subset_3['health'] = df_subset_3['health'].astype("category")
    df_subset_3['steward'] = df_subset_3['steward'].astype("category")

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_3["diam_category"] = df_subset_3.apply(lambda x: 1 if int(x["diam"]) > 10 else 0, axis=1)

    # убираем характеристику - ширина ствола в дюймах
    df_subset_3.drop(["diam"], axis=1, inplace=True)
    # убираем характеристику - идентификатор дерева
    df_subset_3.drop(["tree_id"], axis=1, inplace=True)
    # устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
    df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")

    print(len(df_subset_3))

    return df_subset_3


def step_two(cleaned_data):
    import pandas as pd

    print("step_two")

    # for hydra
    from hydra import compose, initialize
    from omegaconf import OmegaConf
    import logging
    log = logging.getLogger(__name__)
    initialize(version_base=None, config_path="../../src/conf", job_name="test_models")
    cfg = compose(config_name="config", overrides=["preprocessing={}".format("ds_1")])
    log.info(OmegaConf.to_yaml(cfg))

    df2 = cleaned_data[list(cfg['preprocessing']['columns'])]

    train = df2.sample(frac=cfg['preprocessing']['fraction_to_select'], random_state=42)
    test = df2.drop(train.index)
    train_df = train
    test_df = test

    return train_df, test_df


def step_three(train_df, model_name='xgb'):
    import pandas as pd

    def convert_df_with_categorial(dfwc, cat_features):
        import pandas as pd
        return pd.get_dummies(dfwc, prefix=cat_features, columns=cat_features)

    def train(model_name, train_df):
        # for hydra
        from hydra import compose, initialize
        from omegaconf import OmegaConf
        from hydra.utils import instantiate
        import logging
        log = logging.getLogger(__name__)

        initialize(version_base=None, config_path="../../src/conf", job_name="test_models")
        # initialize hydra conf
        cfg = compose(config_name="config", overrides=["model={}".format(model_name)])
        log.info(OmegaConf.to_yaml(cfg))
        print("in modeling 0")

        TARGET_FEATURE = "diam_category"
        cat_features = list(filter(lambda item: item != TARGET_FEATURE, train_df.columns))
        train_df = train_df.astype("category")
        train_df[TARGET_FEATURE] = train_df[TARGET_FEATURE].astype(int)
        print("in modeling 1")

        x_train = convert_df_with_categorial(train_df.drop(TARGET_FEATURE, axis=1), cat_features)
        y_train = train_df[TARGET_FEATURE]
        print("in modeling 2")

        # instanitiate by hydra
        model = instantiate(cfg['model'], random_state=42)
        print("in modeling 3")

        model.fit(x_train, y_train)
        print("in modeling 4")

        return model

    print("step_three")
    model = train(model_name, train_df)
    return model


def step_four(model, test_df, model_name="xgb"):
    print("step_four")
    import pandas as pd

    def convert_df_with_categorial(dfwc, cat_features):
        import pandas as pd
        return pd.get_dummies(dfwc, prefix=cat_features, columns=cat_features)

    def test(model_name, model, test_df):
        from sklearn.metrics import mean_absolute_error, mean_squared_error, roc_auc_score, accuracy_score, f1_score, \
            precision_score, confusion_matrix
        # for hydra
        from hydra import compose, initialize
        from omegaconf import OmegaConf
        import logging
        log = logging.getLogger(__name__)

        initialize(version_base=None, config_path="../../src/conf", job_name="test_models")
        # initialize hydra conf
        cfg = compose(config_name="config", overrides=["model={}".format(model_name)])
        log.info(OmegaConf.to_yaml(cfg))
        print("in testing 0")
        TARGET_FEATURE = "diam_category"
        cat_features = list(filter(lambda item: item != TARGET_FEATURE, test_df.columns))

        test_df = test_df.astype("category")
        test_df[TARGET_FEATURE] = test_df[TARGET_FEATURE].astype(int)
        print("in testing 1")

        x_holdout = convert_df_with_categorial(test_df.drop(TARGET_FEATURE, axis=1), cat_features)
        y_holdout = test_df[TARGET_FEATURE]
        print("in testing 2")

        y_predicted = model.predict(x_holdout)
        score = dict(
            mae=round(mean_absolute_error(y_holdout, y_predicted), 5),
            rmse=round(mean_squared_error(y_holdout, y_predicted), 5),
            roc_auc=round(roc_auc_score(y_holdout, y_predicted, average="macro"), 5),
            accuracy=round(accuracy_score(y_holdout, y_predicted), 5),
            precision=round(precision_score(y_holdout, y_predicted), 5),
            f1=round(f1_score(y_holdout, y_predicted), 5)
        )
        print("in testing 3")

        return score

    metrics = test(model_name, model, test_df)
    return metrics


if __name__ == '__main__':

    # create the pipeline controller
    pipe = PipelineController(
        project='Tree investigation',
        name='Pipeline func',
        version='1.1',
        add_pipeline_tags=False,
    )

    # set the default execution queue to be used (per step we can override the execution)
    pipe.set_default_execution_queue('default')

    # add pipeline components
    pipe.add_parameter(
        name="dataset_name",
        default="Raw data"
    )
    pipe.add_parameter(
        name="dataset_project",
        default="Tree investigation"
    )
    # pipe.add_function_step(
    #     name='step_zero',
    #     function=run_first_pipeline,
    #     function_kwargs=dict(dataset_name='${pipeline.dataset_name}', dataset_project='${pipeline.dataset_project}'),
    #     function_return=['train_df', 'test_df'],
    #     cache_executed_step=True,
    # )
    pipe.add_function_step(
        name='step_one',
        function=step_one,
        function_kwargs=dict(dataset_name='${pipeline.dataset_name}', dataset_project='${pipeline.dataset_project}'),
        function_return=['cleaned_data'],
        cache_executed_step=True,
    )
    pipe.add_function_step(
        name='step_two',
        parents=['step_one'],  # the pipeline will automatically detect the dependencies based on the kwargs inputs
        function=step_two,
        function_kwargs=dict(cleaned_data='${step_one.cleaned_data}'),
        function_return=['train_df', 'test_df'],
        cache_executed_step=True,
    )
    pipe.add_function_step(
        name='step_three',
        parents=['step_two'],  # the pipeline will automatically detect the dependencies based on the kwargs inputs
        function=step_three,
        function_kwargs=dict(train_df='${step_two.train_df}', model_name='xgb'),
        function_return=['model'],
        cache_executed_step=True,
    )
    pipe.add_function_step(
        name='step_four',
        parents=['step_three', 'step_two'],
        function=step_four,
        function_kwargs=dict(model='${step_three.model}', test_df='${step_two.test_df}', model_name='xgb'),
        function_return=['metrics'],
        cache_executed_step=True,
    )

    # For debugging purposes run on the pipeline on current machine
    # Use run_pipeline_steps_locally=True to further execute the pipeline component Tasks as subprocesses.
    pipe.start_locally(run_pipeline_steps_locally=True)

    # Start the pipeline on the services queue (remote machine, default on the clearml-server)
    # pipe.start()

    print('pipeline completed')