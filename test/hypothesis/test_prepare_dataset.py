from hypothesis import given, example, assume, settings
from hypothesis.strategies import text, composite
import hypothesis.strategies as st
from hypothesis.extra.pandas import columns, data_frames, column
import pandas as pd

import unittest


def prepare_tree_dataset(df: pd.DataFrame):
    df_subset = df.copy()
    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
                                               axis=1)
    df_subset.drop(["stump_diam", "tree_dbh"], axis=1, inplace=True)

    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
    # убираем поле статуса дерева
    df_subset_2.drop(["status"], axis=1, inplace=True)
    # также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
    df_subset_2.drop(["problems"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
    df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
    df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
    df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
    df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
    df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
    df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
    df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
    df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
    df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
    df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
    df_subset_3['health'] = df_subset_3['health'].astype("category")
    df_subset_3['steward'] = df_subset_3['steward'].astype("category")

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_3["diam_category"] = df_subset_3.apply(lambda x: 1 if int(x["diam"]) > 10 else 0, axis=1)

    # убираем характеристику - ширина ствола в дюймах
    df_subset_3.drop(["diam"], axis=1, inplace=True)
    # убираем характеристику - идентификатор дерева
    df_subset_3.drop(["tree_id"], axis=1, inplace=True)
    # устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
    df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")

    print(len(df_subset_3))
    return df_subset_3


def prepare_tree_dataset_naive(df: pd.DataFrame):
    df_subset = df.copy()
    # убираем данные с неизвестной шириной ствола или пня
    df_subset.drop(df_subset[(df_subset.stump_diam == float('inf')) |
                             (df_subset.stump_diam == -float('inf')) |
                             (df_subset.tree_dbh == float('inf')) |
                             (df_subset.tree_dbh == -float('inf'))].index,
                   # axis=0,
                   inplace=True)

    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
                                               axis=1)
    df_subset.drop(["stump_diam", "tree_dbh"], axis=1, inplace=True)

    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
    # убираем поле статуса дерева
    df_subset_2.drop(["status"], axis=1, inplace=True)
    # также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
    df_subset_2.drop(["problems"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
    df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
    df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
    df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
    df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
    df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
    df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
    df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
    df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
    df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
    df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
    df_subset_3['health'] = df_subset_3['health'].astype("category")
    df_subset_3['steward'] = df_subset_3['steward'].astype("category")

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_3["diam_category"] = df_subset_3["diam"].apply(lambda x: 1 if int(x) > 10 else 0) # , axis=1

    # убираем характеристику - ширина ствола в дюймах
    df_subset_3.drop(["diam"], axis=1, inplace=True)
    # убираем характеристику - идентификатор дерева
    df_subset_3.drop(["tree_id"], axis=1, inplace=True)
    # устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
    df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")

    # print(len(df_subset_3))
    return df_subset_3


def prepare_tree_dataset_new(df: pd.DataFrame):
    df_subset = df.copy()
    # убираем данные с неизвестной шириной ствола или пня
    df_subset.drop(df_subset[(df_subset.stump_diam == float('inf')) |
                             (df_subset.stump_diam == -float('inf')) |
                             (df_subset.tree_dbh == float('inf')) |
                             (df_subset.tree_dbh == -float('inf'))].index,
                   # axis=0,
                   inplace=True)

    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
                                               axis=1)
    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_2["spc_latin"] = df_subset_2["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_2["diam_category"] = df_subset_2["diam"].apply(lambda x: 1 if int(x) > 10 else 0)  # , axis=1

    # убираем характеристику - ширина ствола в дюймах
    # убираем характеристику - идентификатор дерева
    # также убираем поле problems, как представление в другом виде отдельно вынесенных
    #       полей по проблемам с корнями, стволом и ветвями
    # убираем поле статуса дерева
    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset_2.drop(["diam", "tree_id", "problems", "status", "stump_diam", "tree_dbh"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3 = df_subset_3.astype({'root_stone': 'category',
                                      'root_grate': 'category',
                                      'root_other': 'category',
                                      'trunk_wire': 'category',
                                      'trnk_light': 'category',
                                      'trnk_other': 'category',
                                      'brch_light': 'category',
                                      'brch_shoe': 'category',
                                      'brch_other': 'category',
                                      'sidewalk': 'category',
                                      'curb_loc': 'category',
                                      'health': 'category',
                                      'steward': 'category',
                                      'spc_latin': 'category',
                                      'diam_category': 'int'})

    # print(len(df_subset_3))
    return df_subset_3


@composite
def prepare_dataset_strategy(draw,
                             elements=data_frames(
                                 columns=[column(name='tree_id',    dtype=int, elements=st.integers(min_value=1, max_value=1000)),
                                          column(name='tree_dbh',   dtype=float, elements=st.floats(allow_nan=False)),
                                          column(name='stump_diam', dtype=float, elements=st.floats(allow_nan=False)),
                                          column(name='curb_loc',   dtype=str, elements=st.sampled_from(['OnCurb', 'OffsetFromCurb'])),
                                          column(name='status',     dtype=str, elements=st.sampled_from(['Alive', 'Stump', 'Dead'])),
                                          column(name='health',     dtype=str, elements=st.sampled_from(['Good', 'Fair', 'Poor', None])),
                                          column(name='spc_latin',  dtype=str, elements=st.text(min_size=0, max_size=150)),
                                          column(name='steward',    dtype=str, elements=st.sampled_from(['1or2', '2or3', '4orMore', None])),
                                          column(name='sidewalk',   dtype=str, elements=st.sampled_from(['NoDamage', 'Damage'])),
                                          column(name='problems',   dtype=str, elements=st.text(min_size=0, max_size=150)),
                                          column(name='root_stone', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='root_grate', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='root_other', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='trunk_wire', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='trnk_light', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='trnk_other', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='brch_light', dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='brch_shoe',  dtype=str, elements=st.sampled_from(['True', 'False'])),
                                          column(name='brch_other', dtype=str, elements=st.sampled_from(['True', 'False']))
                                    ],
                                 rows=st.tuples(st.integers(min_value=1, max_value=1000),
                                          st.floats(allow_nan=False),
                                          st.floats(allow_nan=False),
                                          st.sampled_from(['OnCurb', 'OffsetFromCurb']),
                                          st.sampled_from(['Alive', 'Stump', 'Dead']),
                                          st.sampled_from(['Good', 'Fair', 'Poor', None]),
                                          st.text(min_size=0, max_size=150),
                                          st.sampled_from(['1or2', '2or3', '4orMore', None]),
                                          st.sampled_from(['NoDamage', 'Damage']),
                                          st.text(min_size=0, max_size=150),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']),
                                          st.sampled_from(['True', 'False']))
                             )
                            ):
    return draw(elements)


class TestPrepareDataSet(unittest.TestCase):
    @given(prepare_dataset_strategy())
    # @settings(report_multiple_bugs=True)
    @settings(max_examples=100)
    def test_prepare_tree_dataset(self, ds):
        res = prepare_tree_dataset_new(ds)
        self.assertGreaterEqual(len(res['diam_category']), 0)
        if len(res) > 0:
            self.assertIsNotNone(res['spc_latin'].iloc(0), None)
            self.assertEqual(res.dtypes['sidewalk'], "category")

    @given(prepare_dataset_strategy())
    @settings(max_examples=10)
    def test_oracle_prepare_tree_dataset(self, ds):
        df1 = prepare_tree_dataset_naive(ds)
        df2 = prepare_tree_dataset_new(ds)
        diff = df1.compare(df2)
        self.assertEqual(len(diff), 0)


if __name__ == '__main__':
     unittest.main(verbosity=2)