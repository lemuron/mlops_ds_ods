import click
import pandas as pd
from typing import List
import logging
# for hydra
from hydra import compose, initialize
from omegaconf import OmegaConf
# for dvc
import dvc.api


log = logging.getLogger(__name__)

@click.command()
@click.argument("dataset", type=str)
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_paths", type=click.Path(), nargs=2)
def prepare_datasets(dataset: str, input_filepath, output_file_paths: List[str]):

    initialize(version_base=None, config_path="../conf", job_name="prepare_datasets")
    cfg = compose(config_name="config", overrides=["preprocessing={}".format(dataset)])
    log.info(OmegaConf.to_yaml(cfg))

    df = pd.read_csv(input_filepath)
    df2 = df[list(cfg['preprocessing']['columns'])]

    params = dvc.api.params_show()
    print(params)

    train = df2.sample(frac=cfg['preprocessing']['fraction_to_select'], random_state=params["random_state"])
    test = df2.drop(train.index)

    train.to_csv(output_file_paths[0], index=False)
    test.to_csv(output_file_paths[1], index=False)


if __name__ == "__main__":
    prepare_datasets()

