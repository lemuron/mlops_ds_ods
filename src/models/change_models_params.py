from ruamel.yaml import YAML
from pathlib import Path

yaml = YAML()
path_log = Path('src/conf/model/log.yaml')

data = yaml.load(path_log)
data['C'] = 1
yaml.dump(data, path_log)

path_xgb = Path('src/conf/model/xgb.yaml')

data2 = yaml.load(path_xgb)
data2['max_depth'] = 70
yaml.dump(data2, path_xgb)

