import os
import json
import click
import pandas as pd
import joblib as jb
from typing import List
from sklearn.metrics import mean_absolute_error, mean_squared_error, roc_auc_score, accuracy_score, f1_score, precision_score, confusion_matrix
import logging
# hydra
from hydra import compose, initialize
from omegaconf import OmegaConf
from hydra.utils import instantiate
# dvc
import dvc.api
# for dvc plot
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.metrics import (
    ConfusionMatrixDisplay,
)

# mlflow experiments
from dotenv import load_dotenv
import mlflow
from mlflow.models.signature import infer_signature
from mlflow.tracking import MlflowClient

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
print("MLFLOW_TRACKING_URI:".join(remote_server_uri))
mlflow.set_tracking_uri(remote_server_uri)

# for hydra
log = logging.getLogger(__name__)

TARGET_FEATURE = "diam_category"


def convert_df_with_categorial(dfwc, cat_features):
    """
    Function returns input DataSet with converted categorial columns
    :param dfwc: Input DataSet
    :param cat_features: Categorial columns
    :return:
    """
    return pd.get_dummies(dfwc, prefix=cat_features, columns=cat_features)

# visualize in dvc
def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title(f"Confusion Matrix")
    plt.tight_layout()
    return fig

@click.command()
@click.argument("model_name", type=str)
@click.argument("input_paths", type=click.Path(exists=True), nargs=2)
@click.argument("output_paths", type=click.Path(), nargs=-1)
def train(model_name: str, input_paths: List[str], output_paths: List[str]):
    client = MlflowClient()
    experiment_details = client.get_experiment_by_name("DS_ODS")
    with mlflow.start_run(experiment_id=experiment_details.experiment_id): # experiment_id="Default"
        mlflow.get_artifact_uri()

        train_df = pd.read_csv(input_paths[0])
        test_df = pd.read_csv(input_paths[1])

        # initialize hydra conf
        initialize(version_base=None, config_path="../conf", job_name="train_models")
        cfg = compose(config_name="config", overrides=["model={}".format(model_name)])
        log.info(OmegaConf.to_yaml(cfg))

        print("in modeling 0")

        cat_features = list(filter(lambda item: item != TARGET_FEATURE, test_df.columns))

        train_df = train_df.astype("category")
        train_df[TARGET_FEATURE] = train_df[TARGET_FEATURE].astype(int)

        test_df = test_df.astype("category")
        test_df[TARGET_FEATURE] = test_df[TARGET_FEATURE].astype(int)

        print("in modeling 1")

        x_train = convert_df_with_categorial(train_df.drop(TARGET_FEATURE, axis=1), cat_features)
        y_train = train_df[TARGET_FEATURE]
        x_holdout = convert_df_with_categorial(test_df.drop(TARGET_FEATURE, axis=1), cat_features)
        y_holdout = test_df[TARGET_FEATURE]

        print("in modeling 2")

        # initialize dvc params
        params = dvc.api.params_show()
        # instanitiate by hydra
        model = instantiate(cfg['model'], random_state=params['random_state'])

        print("in modeling 3")

        model.fit(x_train, y_train)

        jb.dump(model, output_paths[0])

        print("in modeling 4")

        y_predicted = model.predict(x_holdout)
        score = dict(
            mae=round(mean_absolute_error(y_holdout, y_predicted), 5),
            rmse=round(mean_squared_error(y_holdout, y_predicted), 5),
            roc_auc=round(roc_auc_score(y_holdout, y_predicted, average="macro"), 5),
            accuracy=round(accuracy_score(y_holdout, y_predicted), 5),
            precision=round(precision_score(y_holdout, y_predicted), 5),
            f1=round(f1_score(y_holdout, y_predicted),5)
        )

        print("metrics.json path:" + output_paths[1])

        with open(output_paths[1], "w") as score_file:
            json.dump(score, score_file, indent=4)

        # visualize for dvc
        if len(output_paths) > 2:
            print("confusion matrix plot path:" + output_paths[1])
            fig = conf_matrix(y_holdout, y_predicted)
            plt.savefig(output_paths[2])

        signature = infer_signature(x_holdout, y_predicted)

        mlflow.log_params(params)
        mlflow.log_metrics(score)
        if model_name == "xgb":
            mlflow.xgboost.log_model(xgb_model=model,
                                      artifact_path="model",
                                      registered_model_name="real_estate_xgboost",
                                      signature=signature)
        elif model_name == "log":
            mlflow.sklearn.log_model(sk_model=model,
                                     artifact_path="log_model",
                                     registered_model_name="real_estate_log",
                                     signature=signature)

    experiment = dict(mlflow.get_experiment_by_name("DS_ODS"))  # client.get_experiment_by_name("Default")
    experiment_id = experiment['experiment_id']
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, 'run_id']
    print(best_run_id)

    # если в качестве репозитория для хранения артефактов выбран gitlab, записываем в него модель
    if os.environ["MLFLOW_TRACKING_URI"].__contains__("gitlab"):
        client.log_artifact(best_run_id, output_paths[0], artifact_path="")


if __name__ == "__main__":
    train()

