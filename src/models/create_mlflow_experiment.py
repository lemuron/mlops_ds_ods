import os
import mlflow
from mlflow.tracking import MlflowClient
from dotenv import load_dotenv

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
print("MLFLOW_TRACKING_URI:".join(remote_server_uri))
mlflow.set_tracking_uri(remote_server_uri)

client = MlflowClient()

try:
    experiment_details = client.get_experiment_by_name("DS_ODS")

    if experiment_details is None:
        client.create_experiment("DS_ODS")
except:
    client.create_experiment("DS_ODS")