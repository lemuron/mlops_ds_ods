import os
import sys
import dotenv

if __name__ == "__main__":
    dotenv_file = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_file)

    os.environ["MLFLOW_TRACKING_TOKEN"] = sys.argv[1]
    os.environ["MLFLOW_TRACKING_URI"] = sys.argv[2]

    # Write changes to .env file.
    dotenv.set_key(dotenv_file, "MLFLOW_TRACKING_TOKEN", os.environ["MLFLOW_TRACKING_TOKEN"])
    dotenv.set_key(dotenv_file, "MLFLOW_TRACKING_URI", os.environ["MLFLOW_TRACKING_URI"])

