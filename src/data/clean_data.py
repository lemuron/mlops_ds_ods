"""
Clean data
"""

import pandas as pd
import click

@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function removes excess columns and enforces
    correct data types.
    :param input_path: Path to read DataFrame
    :param output_path: Path to save cleaned DataFrame
    :return:
    """
    df = pd.read_csv(input_path)

    # оставляем наиболее информативные поля
    #     (в первой итерации убираем адресную составляющую, категорию описывающего пользователя, общее название вида и наличие защиты)
    df_subset_0 = df[['tree_id', 'tree_dbh', 'stump_diam',
                    'curb_loc', 'status', 'health', 'spc_latin', 'steward',
                    'sidewalk', 'problems', 'root_stone',
                    'root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
                    'brch_light', 'brch_shoe', 'brch_other', ]]

    df_subset = df_subset_0.copy()
    # оставляем одно поле диаметра ствола или диаметра пня
    df_subset["diam"] = df_subset.copy().apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"],
                                               axis=1)
    df_subset.drop(["stump_diam", "tree_dbh"], axis=1, inplace=True)

    # информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
    df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
    # убираем поле статуса дерева
    df_subset_2.drop(["status"], axis=1, inplace=True)
    # также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
    df_subset_2.drop(["problems"], axis=1, inplace=True)

    df_subset_3 = df_subset_2.copy()
    # превращаем в числовое представление категориальные поля
    df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
    df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
    df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
    df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
    df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
    df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
    df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
    df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
    df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
    df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
    df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
    df_subset_3['health'] = df_subset_3['health'].astype("category")
    df_subset_3['steward'] = df_subset_3['steward'].astype("category")

    # заменяем также отсутствующее наименование вида дерева на константу Not Applicable
    df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")

    # вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
    df_subset_3["diam_category"] = df_subset_3.apply(lambda x: 1 if int(x["diam"]) > 10 else 0, axis=1)

    # убираем характеристику - ширина ствола в дюймах
    df_subset_3.drop(["diam"], axis=1, inplace=True)
    # убираем характеристику - идентификатор дерева
    df_subset_3.drop(["tree_id"], axis=1, inplace=True)
    # устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
    df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")

    print(len(df_subset_3))

    df_subset_3.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()