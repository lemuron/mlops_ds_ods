# mlops_ds_ods


## Создание образа контейнера для среды разработки

```
sudo docker build --target development -t poetry-dev ./dockerfile
```

## Запуск контейнера для среды разработки

```
sudo docker run -d -p 8022:8022 --name poetry-dev --restart=always poetry-dev:latest
```

## Создание образа контейнера для промышленной среды

```
sudo docker build --target production -t poetry-prod ./dockerfile
```

## Запуск контейнера в промышленной среде

```
sudo docker run -d -p 8023:8023 --name poetry-prod --restart=always poetry-prod:latest
```
