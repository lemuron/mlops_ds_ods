---
title: "data_model"
format:
  html:
    code-fold: true
jupyter: python3
---

## Modeling

```{python}
#| label: 2015-street-census-tree-data data_set_columns_info
#| fig-cap: "Modeling data_set_columns_info"

"""
tree_id	  		Уникальный идентификационный номер для каждой точки дерева.
block_id	    Идентификатор, связывающий каждое дерево с блоком в таблице/шейп-файле blockface, на который оно нанесено.
created_at      Точки дерева дат были собраны с помощью программного обеспечения для переписи.
tree_dbh	    Диаметр дерева, измеренный на высоте приблизительно 54 дюйма/137 см над землей.
                Данные были собраны как для живых, так и для мертвых деревьев; для пней используйте stump_diam
stump_diam      Диаметр пня, измеренный через центр, округленный с точностью до дюйма.
curb_loc	    Расположение клумбы по отношению к бордюру; деревья располагаются либо вдоль бордюра (OnCurb), либо смещены от бордюра (OffsetFromCurb).
status	        Указывает, является ли дерево живым, мертвым или представляет собой пень.
health	        Указывает на то, как пользователь оценивает состояние дерева.
spc_latin	    Научное название вида, например "Acer rubrum"
spc_common      Общее название вида, например "красный клен".
steward	        Указывает количество уникальных признаков охраны, наблюдаемых для данного дерева. Не зарегистрировано для пней или мертвых деревьев.
guards	        Указывает, присутствует ли защита и считает ли пользователь, что она полезна или вредна. Не зарегистрировано для мертвых деревьев и пней
sidewalk	    Указывает, был ли поврежден, треснул или поднят один из тротуарных флажков, непосредственно прилегающих к дереву.
                    Не учитывается для засохших деревьев и пней.
user_type	    Это поле описывает категорию пользователя, который собрал данные по этой точке дерева.
root_stone      Указывает на наличие проблемы с корнем, вызванной брусчаткой в подстилке дерева
root_grate      Указывает на наличие проблемы с корнем, вызванной металлическими решетками в подстилке дерева
root_other      Указывает на наличие других проблем с корнями
trunk_wire      Указывает на наличие проблем со стволом, вызванных проводами или веревкой, обмотанными вокруг ствола
trnk_light      Указывает на наличие проблем со стволом, вызванных освещением, установленным на дереве
trnk_other      Указывает на наличие других проблем со стволом
brch_light      Указывает на наличие проблемы с ветвями, вызванной лампочками (обычно гирляндами) или проводами в ветвях
brch_shoe	    Указывает на наличие проблемы с ветвями, вызванной кроссовками в ветвях
brch_other      Указывает на наличие других проблем с ветвями
address	        Ближайший предполагаемый адрес к дереву
zipcode	        Пятизначный почтовый индекс, в котором расположено дерево
zip_city	    Название города определяется по почтовому индексу. Часто (но не всегда) это то же самое, что и название района.
cb_num	        Доска объявлений, в котором находится древовидная точка
borocode	    Код района, в котором находится древовидная точка
boroname	    Название района, в котором находится древовидная точка
cncldist	    Муниципальный округ, в котором находится три-пойнт
st_assem	    Округ Законодательного собрания штата, в котором расположен три-пойнт
st_senate	    Округ Сената штата, в котором расположен три-пойнт
nta	            Это код Национального округа штата, соответствующий району составления таблицы округов по переписи населения США 2010 года, к которому относится три-пойнт.
nta_name	    Это название NTA, соответствующее области составления таблицы районов по переписи населения США 2010 года, к которой относится древовидная точка.
boro_ct	        Это идентификатор boro_ct для переписного участка, к которому относится древовидная точка.
state	        Для всех объектов присвоено значение "Нью-Йорк"
latitude	    Широта точки в десятичных градусах
longitude	    Долгота точки в десятичных градусах
x_sp	        Координата X в плоскости состояний. Единицами измерения являются футы.
y_sp	        Координата Y в плоскости состояний. Единицами измерения являются футы
"""
```


```{python}
#| label: 2015-street-census-tree-data install requirements
#| fig-cap: "Modeling install requirements"

!pip install opendatasets
!pip install numpy
!pip install pandas
!pip install seaborn
!pip install kaggle
!pip install plotly
!pip install chart-studio
!pip install geopandas
!pip install Shapely
!pip install folium
!pip install mapclassify
!pip install xgboost
```

```{python}
#| label: 2015-street-census-tree-data load_dataset
#| fig-cap: "Modeling load_dataset"

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

pd.set_option('display.max_columns', None)

df = pd.read_csv(
    "./ny-2015-street-tree-census-tree-data/2015-street-tree-census-tree-data.csv")

```

```{python}
#| label: 2015-street-census-tree-data sub_dataset
#| fig-cap: "Modeling sub_dataset"

# оставляем наиболее информативные поля
#     (в первой итерации убираем адресную составляющую, категорию описывающего пользователя, общее название вида и наличие защиты)
df_subset_0 = df[['tree_id',  'tree_dbh', 'stump_diam',
       'curb_loc', 'status', 'health', 'spc_latin', 'steward',
       'sidewalk', 'problems', 'root_stone',
       'root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
       'brch_light', 'brch_shoe', 'brch_other',]]
```

```{python}
#| label: 2015-street-census-tree-data dataset_column_values
#| fig-cap: "Modeling dataset_column_values"

# смотрим на возможные значения полей
print(df_subset_0["tree_dbh"].value_counts())
print("------------------------------------------")
print(df_subset_0["stump_diam"].value_counts())
print("------------------------------------------")
print(df_subset_0["curb_loc"].value_counts())
print("------------------------------------------")
print(df_subset_0["status"].value_counts())
print("------------------------------------------")
print(df_subset_0["health"].value_counts())
print("------------------------------------------")
print(df_subset_0["spc_latin"].value_counts())
print("------------------------------------------")
print(df_subset_0["steward"].value_counts())
print("------------------------------------------")
print(df_subset_0["sidewalk"].value_counts())
print("------------------------------------------")
tree_problems = df_subset_0[['root_stone','root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
       'brch_light', 'brch_shoe', 'brch_other',]]
print(tree_problems.apply(pd.Series.value_counts))
```

```{python}
#| label: 2015-street-census-tree-data dataset_merge_diam_columns
#| fig-cap: "Modeling dataset_merge_diam_columns"

df_subset = df_subset_0.copy()
# оставляем одно поле диаметра ствола или диаметра пня
df_subset["diam"] = df_subset.apply(lambda x: x["stump_diam"] if x["stump_diam"] > 0 else x["tree_dbh"], axis=1).copy()
df_subset['diam'] = df_subset['diam'].astype(int)
df_subset.drop(["stump_diam","tree_dbh"], axis=1, inplace=True)
df_subset.head(5)
```

```{python}
#| label: 2015-street-census-tree-data dataset_null_info_by_dead_and_stump
#| fig-cap: "Modeling dataset_null_info_by_dead_and_stump"

# делаем вывод о том, что для мертвых деревьев нет информации по наименованию
print(df_subset[(df_subset["status"] == "Dead") & (df_subset["spc_latin"].isna() != True)].head(5))
#  видно, что информация о проблемах деревьев есть только по живым деревьям, по мертвым деревьям и пням бОльшая часть информации не фиксируется
tree_problems_1 = df_subset[(df_subset["status"] == "Dead") | (df_subset["status"] == "Stump")][['sidewalk','health','steward','sidewalk','spc_latin','problems','root_stone','root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
       'brch_light', 'brch_shoe', 'brch_other',]]
print(tree_problems_1.apply(pd.Series.value_counts))
# по пням и мертвым деревьям указывается только
#    - расположение клумбы к бордюру
tree_problems_2 = df_subset[(df_subset["status"] == "Dead") | (df_subset["status"] == "Stump")][['curb_loc']]
print(tree_problems_2.apply(pd.Series.value_counts))
#    - диаметр ствола или пня
tree_problems_3 = df_subset[(df_subset["status"] == "Dead") | (df_subset["status"] == "Stump")][['diam']]
print(tree_problems_3.apply(pd.Series.value_counts))

```

```{python}
#| label: 2015-street-census-tree-data dataset_drop_info_by_dead_and_stump
#| fig-cap: "Modeling dataset_drop_info_by_dead_and_stump"

# информацию по мертвым деревьям и пням исключаем из дальнейшего рассмотрения:
df_subset_2 = df_subset[df_subset["status"] == "Alive"].copy()
# убираем поле статуса дерева
df_subset_2.drop(["status"], axis=1, inplace=True)
# также убираем поле problems, как представление в другом виде отдельно вынесенных полей по проблемам с корнями, стволом и ветвями
df_subset_2.drop(["problems"],axis=1,inplace=True)
df_subset_2.info()
```

```{python}
#| label: 2015-street-census-tree-data dataset_categorial_heat_map
#| fig-cap: "Modeling dataset_categorial_heat_map"

df_subset_3 = df_subset_2.copy()
# превращаем в числовое представление категориальные поля
df_subset_3['root_stone'] = df_subset_3['root_stone'].astype("category")
df_subset_3['root_grate'] = df_subset_3['root_grate'].astype("category")
df_subset_3['root_other'] = df_subset_3['root_other'].astype("category")
df_subset_3['trunk_wire'] = df_subset_3['trunk_wire'].astype("category")
df_subset_3['trnk_light'] = df_subset_3['trnk_light'].astype("category")
df_subset_3['trnk_other'] = df_subset_3['trnk_other'].astype("category")
df_subset_3['brch_light'] = df_subset_3['brch_light'].astype("category")
df_subset_3['brch_shoe'] = df_subset_3['brch_shoe'].astype("category")
df_subset_3['brch_other'] = df_subset_3['brch_other'].astype("category")
df_subset_3['sidewalk'] = df_subset_3['sidewalk'].astype("category")
df_subset_3['curb_loc'] = df_subset_3['curb_loc'].astype("category")
df_subset_3['health'] = df_subset_3['health'].astype("category")
df_subset_3['steward'] = df_subset_3['steward'].astype("category")

# посмотрим тепловую карту корреляций отобранных признаков:
# df3 = df_subset_3.drop(["spc_latin"],axis=1) #select_dtypes(exclude=['object'])
# sns.set(font_scale=1.15)
# plt.figure(figsize=(8,4))
# sns.heatmap(
#     df3.corr(),
#     cmap='RdBu_r',    # задаёт цветовую схему
#     annot=True,       # рисует значения внутри ячеек
#     vmin=-1, vmax=1); # указывает начало цветовых кодов от -1 до 1.

# видим, что есть небольшая корреляция между количеством признаков охраны и толщиной ствола дерева,
#     также между толщиной ствола и наличием проблем с корнями
#     и между расположением клумбы относительно бордюра и наличием проблем с корнями
```

```{python}
#| label: 2015-street-census-tree-data dataset_additional_prepare__target
#| fig-cap: "Modeling dataset_additional_prepare__target"

# заменяем также отсутствующее наименование вида дерева на константу Not Applicable
df_subset_3["spc_latin"] = df_subset_3["spc_latin"].fillna("Not Applicable")
# вводим целевой признак - категория по ширине ствола: 0 - большие деревья, 1 - маленькие деревья
df_subset_3["diam_category"] = df_subset_3.apply(lambda x: 1 if int(x["diam"]) > 10 else 0, axis=1)
# убираем характеристику - ширина ствола в дюймах
df_subset_3.drop(["diam"], axis=1, inplace=True)
# убираем характеристику - идентификатор дерева
df_subset_3.drop(["tree_id"], axis=1, inplace=True)
# устанавливаем признак вид дерева с категориальным типом, предварительно убедившись, что видов чуть больше 100
df_subset_3['spc_latin'] = df_subset_3['spc_latin'].astype("category")
df_subset_3["spc_latin"].value_counts()
```

```{python}
#| label: 2015-street-census-tree-data dataset_additional_service_functions
#| fig-cap: "Modeling dataset_additional_service_functions"

from sklearn import metrics

# вводим на всякий случай функцию оценки roc_auc для многоклассовой классификации
def roc_auc_score_multiclass(actual_class, pred_class, average = "macro"):
    #creating a set of all the unique classes using the actual class list
    unique_class = set(actual_class)
    roc_auc_dict = {}
    for per_class in unique_class:
        #creating a list of all the classes except the current class
        other_class = [x for x in unique_class if x != per_class]
        #marking the current class as 1 and all other classes as 0
        new_actual_class = [0 if x in other_class else 1 for x in actual_class]
        new_pred_class = [0 if x in other_class else 1 for x in pred_class]
        #using the sklearn metrics method to calculate the roc_auc_score
        roc_auc = metrics.roc_auc_score(new_actual_class, new_pred_class, average = average)
        roc_auc_dict[per_class] = roc_auc
        print("ROC AUC for class {}: {}".format(per_class, roc_auc))
    return roc_auc_dict

# вводим функцию оценки качества модели с помощью нескольких метрик
def get_metrics(y_test, y_pred):
    # print(y_test)
    # print(y_pred)
    mse_v = metrics.mean_squared_error(y_test, y_pred)
    roc_auc_v =  roc_auc_score_multiclass(y_test, y_pred) # metrics.roc_auc_score(y_test, y_pred, multi_class='ovr')
    accuracy_v = metrics.accuracy_score(y_test, y_pred)
    f1_v = metrics.f1_score(y_test, y_pred, average='weighted')

    print("Accuracy:", accuracy_v)
    print("F1:", f1_v)
    # print("ROC AUC:")
    # print(roc_auc_v)
    print("Mean squared error:", mse_v)

    return mse_v, roc_auc_v, accuracy_v, f1_v

# вводим функцию превращения для категориальных признаков
def convert_df_with_categorial(dfwc, cat_features):
    return pd.get_dummies(dfwc, prefix=cat_features,columns=cat_features)

```

```{python}
#| label: 2015-street-census-tree-data model_xgboost
#| fig-cap: "Modeling model_xgboost"

# импортируем зависимости
from xgboost import XGBClassifier

FEATURES = ['curb_loc', 'health', 'diam_category',
            'spc_latin', 'steward', 'sidewalk',
            'root_stone', 'root_grate', 'root_other',
            'trunk_wire', 'trnk_light', 'trnk_other',
            'brch_light', 'brch_shoe', 'brch_other']

CAT_FEATURES = ['curb_loc', 'health',
            'spc_latin', 'steward', 'sidewalk',
            'root_stone', 'root_grate', 'root_other',
            'trunk_wire', 'trnk_light', 'trnk_other',
            'brch_light', 'brch_shoe', 'brch_other']

df_main = df_subset_3[FEATURES]
# разбиваем выборку на обучение и тест
train_df = df_main.sample(frac=0.75, random_state=200)
test_df = df_main.drop(train_df.index)
# выделяем целевой признак на обучении и тесте
x_train = convert_df_with_categorial(train_df.drop('diam_category', axis=1), CAT_FEATURES)
y_train = train_df['diam_category']
x_holdout = convert_df_with_categorial(test_df.drop('diam_category', axis=1), CAT_FEATURES)
y_holdout = test_df['diam_category']
# инициализируем модель
model_xgb = XGBClassifier(
    use_label_encoder=False,
    eval_metric='mlogloss',
    max_depth=50)
    # enable_categorical = True)
# обучаем модель
model_xgb.fit(x_train, y_train)
# проверяем на тестовой выборке
y_predicted = model_xgb.predict(x_holdout)
# выводим полученные метрики качества модели
print("\nXGB:")
mse_dt_xgb, roc_auc_dt_xgb, accuracy_dt_xgb, f1_dt_xgb = get_metrics(y_holdout, y_predicted)

```

```{python}
#| label: 2015-street-census-tree-data model_logistic_regression
#| fig-cap: "Modeling model_logistic_regression"

from sklearn.linear_model import LogisticRegression

df_main = df_subset_3.copy()
# разбиваем выборку на обучение и тест
train_df = df_main.sample(frac=0.75, random_state=200)
test_df = df_main.drop(train_df.index)
# выделяем целевой признак на обучении и тесте, попутно преобразуем категориальные признаки для корректной обработки
x_train = convert_df_with_categorial(train_df.drop('diam_category', axis=1), CAT_FEATURES)
y_train = train_df['diam_category']
x_holdout = convert_df_with_categorial(test_df.drop('diam_category', axis=1), CAT_FEATURES)
y_holdout = test_df['diam_category']
# инициализируем модель
model_lr = LogisticRegression(random_state=1, penalty='l2', solver='saga', C=0.1)
# обучаем модель
model_lr.fit(x_train, y_train)
# проверяем на тестовой выборке
y_pred_lr = model_lr.predict(x_holdout)
# выводим полученные метрики качества модели
print("\nLogistic regression:")
mse_dt_lr, roc_auc_dt_lr, accuracy_dt_lr, f1_dt_lr = get_metrics(y_holdout, y_pred_lr)

# Вывод: качество моделей невысокое, и вряд ли поможет перебор гиперпараметров, данных недостаточно, необходимо введение дополнительных фичей
#       либо усложнение алгоритма модели. Требуется продолжение исследования, в том числе и с внедрением ранее отброшенных признаков геолокации в том числе
```
